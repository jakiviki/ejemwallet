package com.cristianvillamil.platziwallet.ui.home.data

//////////patron de diseño BUILDER


class User private constructor(private val userName:String,private val password:String) {

    class Builder{
        var username:String = ""
        var password:String? = null
        fun setUsername(username:String):Builder{
            this.username = username
            return this
        }

        fun setPassword(newPassword:String):Builder{
            this.password = newPassword
            return this
        }

        fun buid():User{
            return User(username,password ?: "")
        }

    }

}