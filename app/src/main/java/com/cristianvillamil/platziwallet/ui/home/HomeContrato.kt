package com.cristianvillamil.platziwallet.ui.home

interface HomeContrato {
    interface View {
        fun showLoader()
        fun hindLoader()
        fun showFavoriteTransfer(favoriteTransfer: List<FavoriteTransfer>)
    }

    interface Presenter {
        fun retrieveFavoriteTransfer(): List<FavoriteTransfer>
    }
}