package com.cristianvillamil.platziwallet

// este patron devuelve una instancia para que solo se cree una instancia de esta clase
// la variable tiene que ser estatica al igual que el metodo
// para esto en kotlin lo hacemos con companion objet

// la llamo en el HomePresenter



class UserSingleton {

    // variable de ejemplo para acceder a ella
    var nombre: String = "javi"

    // lo hacemos estatico.
    companion object{
        private lateinit var instance: UserSingleton

        fun getInstance(): UserSingleton{
            if (instance == null){
                instance = UserSingleton()
            }
            return instance
        }
    }


}